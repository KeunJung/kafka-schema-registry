package com.kotlin.kafka.sample.controller

import com.kotlin.kafka.sample.service.Producer
import model.User
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class KafkaController(
        private val producer: Producer
        ) {

    @PostMapping(value = ["/publish"])
    fun sendMessageToKafka(@RequestBody user: User) = producer.sendMessage(user)
}
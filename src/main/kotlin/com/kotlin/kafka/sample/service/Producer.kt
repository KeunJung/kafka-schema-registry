package com.kotlin.kafka.sample.service

import com.kotlin.kafka.sample.config.KafkaProperties
import model.User
import org.apache.kafka.clients.producer.ProducerRecord
import org.slf4j.LoggerFactory
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Service

@Service
class Producer(
        private val kafkaTemplate: KafkaTemplate<String, User>,
        private val kafkaProperties: KafkaProperties
) {
    private val LOGGER = LoggerFactory.getLogger(Producer::class.java)

    fun sendMessage(user: User) {
        val producerRecord = ProducerRecord<String, User>(kafkaProperties.topicName, user)
        LOGGER.info("#### -> Producing message -> $user")
        kafkaTemplate.send(producerRecord)
    }
}
package com.kotlin.kafka.sample.service

import model.User
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.slf4j.LoggerFactory
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Service

@Service
class Consumer {
    private val LOGGER = LoggerFactory.getLogger(Consumer::class.java)

    @KafkaListener(topics = ["users"], groupId = "test")
    fun consume(record: ConsumerRecord<String, User>) {
        val user = record.value();

        LOGGER.info("Name ${user.getName()}")
        LOGGER.info("#### -> Consumed message -> $user")
    }
}
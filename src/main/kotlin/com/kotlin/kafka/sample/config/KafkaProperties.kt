package com.kotlin.kafka.sample.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "kafka-properties")
class KafkaProperties {
        var partitions: Int = 0
        var replicationFactor: Short = 0
        var topicName: String = ""
}

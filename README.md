# Kafka schema registry

### Before Running server

1. Kotlin plugin install

2. Terminal run script
```shell
./kafka-up.sh
```

### Additional Links

* [Wiki - Guides](https://mz-dev.atlassian.net/wiki/spaces/SBT/pages/701005867/Schema+Registry)
* [Apache avro](http://avro.apache.org/docs/current/spec.html)
* [Confluent platform](https://docs.confluent.io/platform/current/overview.html)

